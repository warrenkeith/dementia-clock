module.exports = function(grunt) {

	grunt.initConfig({
		compass: {
			all: {
				options: {
					sassDir: 'sass',
					cssDir: 'css'
				}
			}
		},
		watch: {
			styles: {
				files: 'sass/*.scss',
				tasks: ['compass'],
                options: {
                    livereload: true
                },
            },
            livereload: {
                options: {
                    livereload: true
                },
                files: ['*.html','js/*.js']
            }
        },
    connect: {
        server: {
            options: {
                port: 9000,
                hostname: 'localhost',
                base: '',
                open: true
            }
        }
    }
});

    grunt.loadNpmTasks('grunt-contrib-compass');
    grunt.loadNpmTasks('grunt-contrib-watch');
    grunt.loadNpmTasks('grunt-contrib-connect');

    grunt.registerTask('default',['compass','connect','watch']);

};