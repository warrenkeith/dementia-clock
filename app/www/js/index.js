var app = {

    addZero: function(i) {
        if (i < 10) {
            i = "0" + i;
        }
        return i;
    },

    vars: function(){
        var
        timeofDay,
        time,
        day,
        message;
    },

    getHour: function() {
        var d = new Date();
        var hh = d.getHours();
        var mm = this.addZero(d.getMinutes());
        var dd = "am";
        var h = hh;
        if (h >= 12) {
            h = hh-12;
            dd = "pm";
        }
        if (h === 0) {
            h = 12;
        }
        time = h+':'+mm+dd;
    },

    getTheDay: function() {
        switch (new Date().getDay()) {
            case 0:
            day = "Sunday.";
            break;
            case 1:
            day = "Monday.";
            break;
            case 2:
            day = "Tuesday.";
            break;
            case 3:
            day = "Wednesday.";
            break;
            case 4:
            day = "Thursday.";
            break;
            case 5:
            day = "Friday.";
            break;
            case 6:
            day = "Saturday.";
            break;
        }
    },

    getTimeOfDay: function() {
        switch (new Date().getHours()) {
            case 23:
            case 0:
            timeOfDay = "It's late at night.";
            break;
            case 1:
            case 2:
            case 3:
            case 4:
            timeOfDay = "It's the middle of the night.";
            break;
            case 5:
            case 6:
            case 7:
            timeOfDay = "It's very early in the morning.";
            break;
            case 8:
            case 9:
            timeOfDay = "It's morning.";
            break;
            case 10:
            case 11:
            timeOfDay = "It's late morning.";
            break;
            case 12:
            case 13:
            timeOfDay = "It's early afternoon.";
            break;
            case 14:
            case 15:
            timeOfDay = "It's mid-afternoon.";
            break;
            case 16:
            case 17:
            timeOfDay = "It's late afternoon.";
            break;
            case 18:
            case 19:
            timeOfDay = "It's evening.";
            break;
            case 20:
            case 21:
            case 22:
            timeOfDay = "It's night-time.";
            break;
        }
    },

    getTimes: function(){
        this.vars();
        this.getTheDay();
        this.getHour();
        this.getTimeOfDay();
        $('#day').html(day);
        $('#time').html(time);
        $('#timeOfDay').html(timeOfDay);
    },

    analog: function() {
          var seconds = new Date().getSeconds();
          var sdegree = seconds * 6;
          var srotate = "rotate(" + sdegree + "deg)";
          $("#sec").css({ "transform": srotate });
          var hours = new Date().getHours();
          var mins = new Date().getMinutes();
          var hdegree = hours * 30 + (mins / 2);
          var hrotate = "rotate(" + hdegree + "deg)";
          $("#hour").css({ "transform": hrotate});
          var mins = new Date().getMinutes();
          var mdegree = mins * 6;
          var mrotate = "rotate(" + mdegree + "deg)";
          $("#min").css({ "transform" : mrotate });
    },

	cssClock: function () {
		/*
		 * Starts any clocks using the user's local time
		 * From: cssanimation.rocks/clocks
		 */
		function initLocalClocks() {
			// Get the local time using JS
			var date = new Date;
			var seconds = date.getSeconds();
			var minutes = date.getMinutes();
			var hours = date.getHours();

			// Create an object with each hand and it's angle in degrees
			var hands = [
				{
					hand: 'hours',
					angle: (hours * 30) + (minutes / 2)
				},
				{
					hand: 'minutes',
					angle: (minutes * 6)
				},
				{
					hand: 'seconds',
					angle: (seconds * 6)
				}
			];
			// Loop through each of these hands to set their angle
			for (var j = 0; j < hands.length; j++) {
				var elements = document.querySelectorAll('.' + hands[j].hand);
				for (var k = 0; k < elements.length; k++) {
					elements[k].style.webkitTransform = 'rotateZ(' + hands[j].angle + 'deg)';
					elements[k].style.transform = 'rotateZ(' + hands[j].angle + 'deg)';
					// If this is a minute hand, note the seconds position (to calculate minute position later)
					if (hands[j].hand === 'minutes') {
						elements[k].parentNode.setAttribute('data-second-angle', hands[j + 1].angle);
					}
				}
			}
		}

		initLocalClocks();
	},

    init: function() {
        var me = this;
	    me.cssClock();
	    // re-run this function once an hour
        window.setInterval(function(){
	        me.cssClock();
        }, 43200);



    }
};
